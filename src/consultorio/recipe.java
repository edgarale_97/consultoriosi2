/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultorio;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author NB 3300
 */
public class recipe extends javax.swing.JFrame implements Printable {

    conexion cc = new conexion();
    Connection cn= cc.conectar();
    ResultSet rs; 
    
    public recipe() {
        initComponents();
        this.setLocationRelativeTo(null);
        txtIdPaciente.setVisible(false);
        txtIdRecipe.setVisible(false);
      }
    void escribir(){
        try{
            PreparedStatement pst = cn.prepareStatement("SELECT * FROM recipe_medico  WHERE idpacientes=?");
            pst.setString(1,txtIdPaciente.getText());
            rs=pst.executeQuery();
            if(rs.next()){
                txtIdRecipe.setText(rs.getString("idRecipe_medico"));
                txtIdPaciente.setText(rs.getString("idpacientes"));
                taMedicina.setText(rs.getString("medicina"));
                taIndicaciones.setText(rs.getString("indicacion"));
                
             }else{
                JOptionPane.showMessageDialog(null, "No existe la historia medica en los registros");
            }
        }catch(Exception ex){
            
        }
    }
    void limpiar(){
         txtIdPaciente.setText(null);
        txtIdRecipe.setText(null);
        txtNombre.setText(null);
        txtapellido.setText(null);
        taMedicina.setText(null);
        taIndicaciones.setText(null);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtIdPaciente = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtapellido = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JScrollPane = new javax.swing.JScrollPane();
        taMedicina = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        JSCrollPane = new javax.swing.JScrollPane();
        taIndicaciones = new javax.swing.JTextArea();
        btGuardar = new javax.swing.JButton();
        btModificar = new javax.swing.JButton();
        btEliminar = new javax.swing.JButton();
        btVolver = new javax.swing.JButton();
        Btimprimir = new javax.swing.JButton();
        txtIdRecipe = new javax.swing.JTextField();
        btCerrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtIdPaciente.setEditable(false);
        getContentPane().add(txtIdPaciente, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 0, 20, -1));
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, 110, -1));
        getContentPane().add(txtapellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, 110, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Recipe del paciente:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 170, -1, -1));

        taMedicina.setColumns(20);
        taMedicina.setRows(5);
        JScrollPane.setViewportView(taMedicina);

        getContentPane().add(JScrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, 300, 500));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Indicaciones");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 170, -1, -1));

        taIndicaciones.setColumns(20);
        taIndicaciones.setRows(5);
        JSCrollPane.setViewportView(taIndicaciones);

        getContentPane().add(JSCrollPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 210, 280, 500));

        btGuardar.setBackground(new java.awt.Color(0, 255, 255));
        btGuardar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btGuardar.setText("Guardar Recipe");
        btGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 260, 130, -1));

        btModificar.setBackground(new java.awt.Color(0, 255, 255));
        btModificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btModificar.setText("Modificar Recipe");
        btModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btModificarActionPerformed(evt);
            }
        });
        getContentPane().add(btModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 310, 130, -1));

        btEliminar.setBackground(new java.awt.Color(0, 255, 255));
        btEliminar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btEliminar.setText("Eliminar Recipe");
        btEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 360, 130, -1));

        btVolver.setBackground(new java.awt.Color(0, 255, 255));
        btVolver.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btVolver.setText("Volver a Datos del Paciente");
        btVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVolverActionPerformed(evt);
            }
        });
        getContentPane().add(btVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 570, 190, -1));

        Btimprimir.setBackground(new java.awt.Color(0, 255, 255));
        Btimprimir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Btimprimir.setText("Imprimir");
        Btimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtimprimirActionPerformed(evt);
            }
        });
        getContentPane().add(Btimprimir, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 650, 150, -1));

        txtIdRecipe.setEditable(false);
        getContentPane().add(txtIdRecipe, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 180, 20, -1));

        btCerrar.setBackground(new java.awt.Color(255, 204, 0));
        btCerrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btCerrar.setText("Cerrar");
        btCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 10, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/acuarela-colorida-fondo-de-textura-de-grunge-fondo-suave_3248-2562.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGuardarActionPerformed
if (JOptionPane.showConfirmDialog(rootPane, "¿Desea Guardar el recipe del paciente?",
          "Guardar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){ 
        
        try {
            
            PreparedStatement pst = cn.prepareStatement("INSERT INTO recipe_medico (idpacientes,medicina,indicacion) VALUES(?,?,?)");
            pst.setString(1, txtIdPaciente.getText());
            pst.setString(2, taMedicina.getText());
            pst.setString(3,taIndicaciones.getText());
            
             
            int executeUpdate = pst.executeUpdate();
           
            if (executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al guardar");
            }else{
                JOptionPane.showMessageDialog(null,"Error al guardar");
            }
        } catch(Exception ex){
            
        }
}
    }//GEN-LAST:event_btGuardarActionPerformed

    private void btModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btModificarActionPerformed
       if (JOptionPane.showConfirmDialog(rootPane, "Se modificara el registro, ¿desea continuar?",
        "Eliminar Registro", JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
        try {
            
            PreparedStatement pst = cn.prepareStatement("UPDATE recipe_medico SET idpacientes=?,medicina=?,indicacion=? WHERE idRecipe_medico=?");
            pst.setString(1, txtIdPaciente.getText());
            pst.setString(2, taMedicina.getText());
            pst.setString(3, taIndicaciones.getText());
            pst.setString(4, txtIdRecipe.getText());
            
             
            int executeUpdate = pst.executeUpdate();
           if(executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al modificar");
            }else{
                JOptionPane.showMessageDialog(null,"Error no se pudo modificar");
            }
        } catch(Exception ex){
            
        }
        }
    }//GEN-LAST:event_btModificarActionPerformed

    private void btEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarActionPerformed
       if (JOptionPane.showConfirmDialog(rootPane, "Se eliminará el recipe medico Guardado, ¿desea continuar?",
        "Eliminar Registro", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
         try{
           
           PreparedStatement ps= cn.prepareStatement("DELETE FROM recipe_medico WHERE idRecipe_medico=?");
            ps.setInt(1,Integer.parseInt(txtIdRecipe.getText()));
             
            int executeUpdate = ps.executeUpdate();
             if(executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al Eliminar el Recipe");
                limpiar();
                
            }else{
                JOptionPane.showMessageDialog(null,"Error no se pudo eliminar el Recipe");
            }
       
       }catch (Exception ex){
           
       }
        }
    }//GEN-LAST:event_btEliminarActionPerformed

    private void BtimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtimprimirActionPerformed
       //Crea y devuelve un printerjob que se asocia con la impresora predeterminada
        //del sistema, si no hay, retorna NULL
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        //Se pasa la instancia del JFrame al PrinterJob
        printerJob.setPrintable(this);
        //muestra ventana de dialogo para imprimir
        if ( printerJob.printDialog())
        {
            try {
                printerJob.print();
            } catch (PrinterException ex) {
            System.out.println("Error:" + ex);
            }
        }        
    }//GEN-LAST:event_BtimprimirActionPerformed

    private void btCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCerrarActionPerformed
       System.exit(0);
    }//GEN-LAST:event_btCerrarActionPerformed

    private void btVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVolverActionPerformed
        if (JOptionPane.showConfirmDialog(rootPane, "Se perderan los datos que no se guardaron del recipe, ¿desea continuar?",
        "Se volvera al formularo del paciente", JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
        registro r = new registro();
        this.setVisible(false);
        r.setVisible(true);
        }
    }//GEN-LAST:event_btVolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(recipe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(recipe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(recipe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(recipe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new recipe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btimprimir;
    private javax.swing.JScrollPane JSCrollPane;
    private javax.swing.JScrollPane JScrollPane;
    private javax.swing.JButton btCerrar;
    private javax.swing.JButton btEliminar;
    private javax.swing.JButton btGuardar;
    private javax.swing.JButton btModificar;
    private javax.swing.JButton btVolver;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextArea taIndicaciones;
    private javax.swing.JTextArea taMedicina;
    public static javax.swing.JTextField txtIdPaciente;
    private javax.swing.JTextField txtIdRecipe;
    public static javax.swing.JTextField txtNombre;
    public static javax.swing.JTextField txtapellido;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
