/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultorio;

import java.sql.*;
import javax.swing.JOptionPane;


public class Estilista extends javax.swing.JFrame {
conexion cc = new conexion();
    Connection cn = cc.conectar();
    ResultSet rs;
    public Estilista() {
        initComponents();
        this.setLocationRelativeTo(null);
        txtIdPaciente.setVisible(false);
        txtIdbelleza.setVisible(false);
    }
    void mostrar(){
         try{
            PreparedStatement pst = cn.prepareStatement("SELECT * FROM belleza WHERE idpaciente=?");
            pst.setString(1,txtIdPaciente.getText());
            rs=pst.executeQuery();
            if(rs.next()){
                txtIdbelleza.setText(rs.getString("idbelleza"));
                txtIdPaciente.setText(rs.getString("idpaciente"));
                txtnomTratamiento.setText(rs.getString("tratamiento_nombre"));
                TaAplicar.setText(rs.getString("aplica"));
                txtFecha.setText(rs.getString("fecha"));
                TaCremas.setText(rs.getString("cremas"));
                }else{
                JOptionPane.showMessageDialog(null, "No existe plan de nutricion");
            }
        }catch(Exception ex){
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtIdPaciente = new javax.swing.JTextField();
        txtapellido = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtnomTratamiento = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TaAplicar = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TaCremas = new javax.swing.JTextArea();
        btGuardar = new javax.swing.JButton();
        btModificar = new javax.swing.JButton();
        btEliminar = new javax.swing.JButton();
        btCerrar = new javax.swing.JButton();
        txtIdbelleza = new javax.swing.JTextField();
        btVolver = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtIdPaciente.setEditable(false);
        getContentPane().add(txtIdPaciente, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, 20, -1));
        getContentPane().add(txtapellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 70, 150, -1));
        getContentPane().add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 40, 150, -1));

        jLabel2.setText("Nombre Del Trataniento:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 150, -1, -1));
        getContentPane().add(txtnomTratamiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 150, 160, -1));

        jLabel3.setText("Aplicacion:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, -1, -1));

        TaAplicar.setColumns(20);
        TaAplicar.setRows(5);
        jScrollPane1.setViewportView(TaAplicar);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 180, -1, -1));

        jLabel4.setText("Fecha:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 290, -1, -1));
        getContentPane().add(txtFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 290, 80, -1));

        jLabel5.setText("Cremas a usar:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 330, -1, -1));

        TaCremas.setColumns(20);
        TaCremas.setRows(5);
        jScrollPane2.setViewportView(TaCremas);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 320, -1, -1));

        btGuardar.setBackground(new java.awt.Color(0, 204, 204));
        btGuardar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btGuardar.setText("Guardar");
        btGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 460, -1, -1));

        btModificar.setBackground(new java.awt.Color(0, 255, 255));
        btModificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btModificar.setText("Modificar Registro");
        btModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btModificarActionPerformed(evt);
            }
        });
        getContentPane().add(btModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 460, -1, -1));

        btEliminar.setBackground(new java.awt.Color(0, 255, 255));
        btEliminar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btEliminar.setText("Eliminar Registro");
        btEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 460, -1, -1));

        btCerrar.setBackground(new java.awt.Color(255, 153, 51));
        btCerrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btCerrar.setText("Cerrar");
        btCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 10, 70, -1));

        txtIdbelleza.setEditable(false);
        getContentPane().add(txtIdbelleza, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 120, 20, -1));

        btVolver.setBackground(new java.awt.Color(0, 255, 255));
        btVolver.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btVolver.setText("Volver al los registro de pacientes");
        btVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVolverActionPerformed(evt);
            }
        });
        getContentPane().add(btVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 460, -1, -1));

        jLabel6.setText("Tratamiento Esteticos:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 80, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/acuarela-colorida-fondo-de-textura-de-grunge-fondo-suave_3248-2562.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCerrarActionPerformed
       if (JOptionPane.showConfirmDialog(rootPane, "¿Seguro que desea salir? se perderan los datos no guardados, ¿desea continuar?",
        "Eliminar Registro", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            System.exit(0);
         }
    }//GEN-LAST:event_btCerrarActionPerformed

    private void btGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGuardarActionPerformed
if (JOptionPane.showConfirmDialog(rootPane, "¿Desea Guardar el tratamiento del paciente?",
          "Guardar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
        try {
            
            PreparedStatement pst = cn.prepareStatement("INSERT INTO belleza (idpaciente,tratamiento_nombre,aplica,fecha,cremas) VALUES(?,?,?,?,?)");
            pst.setString(1, txtIdPaciente.getText());
            pst.setString(2, txtnomTratamiento.getText());
            pst.setString(3, TaAplicar.getText());
            pst.setString(4, txtFecha.getText());
            pst.setString(5, TaCremas.getText());
             
            int executeUpdate = pst.executeUpdate();
           
            if (executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al guardar");
            }else{
                JOptionPane.showMessageDialog(null,"Error al guardar");
            }
        } catch(Exception ex){
            
        }
}
    }//GEN-LAST:event_btGuardarActionPerformed

    private void btModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btModificarActionPerformed
          if (JOptionPane.showConfirmDialog(rootPane, "Se modificara el registro, ¿desea continuar?",
        "Eliminar Registro", JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
        try {
            
            PreparedStatement pst = cn.prepareStatement("UPDATE belleza SET idpaciente=?,tratamiento_nombre=?,aplica=?,fecha=?,cremas=? WHERE idbelleza=?");
            pst.setString(1, txtIdPaciente.getText());
            pst.setString(2, txtnomTratamiento.getText());
            pst.setString(3, TaAplicar.getText());
            pst.setString(4, txtFecha.getText());
            pst.setString(5, TaCremas.getText());
            pst.setString(6, txtIdbelleza.getText());
            int executeUpdate = pst.executeUpdate();
           if(executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al modificar");
            }else{
                JOptionPane.showMessageDialog(null,"Error no se pudo modificar");
            }
        } catch(Exception ex){
            
        }
        }
    }//GEN-LAST:event_btModificarActionPerformed

    private void btEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarActionPerformed
         if (JOptionPane.showConfirmDialog(rootPane, "Se eliminará el registro de la Historia clinica, ¿desea continuar?",
        "Eliminar Registro", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
         try{
           
           PreparedStatement ps= cn.prepareStatement("DELETE FROM belleza WHERE idbelleza=?");
            ps.setInt(1,Integer.parseInt(txtIdbelleza.getText()));
             
            int executeUpdate = ps.executeUpdate();
             if(executeUpdate > 0){
                JOptionPane.showMessageDialog(null,"Exito al Eliminar el telefono");
                
                
            }else{
                JOptionPane.showMessageDialog(null,"Error no se pudo eliminar el telefono");
            }
       
       }catch (Exception ex){
           
       }
        }
    }//GEN-LAST:event_btEliminarActionPerformed

    private void btVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVolverActionPerformed
        if (JOptionPane.showConfirmDialog(rootPane, "Se perderan los datos que no se guardaron de la historia clinica, ¿desea continuar?",
        "Se volvera al formularo del paciente", JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
        registro r = new registro();
        this.setVisible(false);
        r.setVisible(true);
        }
    }//GEN-LAST:event_btVolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Estilista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Estilista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Estilista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Estilista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Estilista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea TaAplicar;
    private javax.swing.JTextArea TaCremas;
    private javax.swing.JButton btCerrar;
    private javax.swing.JButton btEliminar;
    private javax.swing.JButton btGuardar;
    private javax.swing.JButton btModificar;
    private javax.swing.JButton btVolver;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField txtFecha;
    public static javax.swing.JTextField txtIdPaciente;
    private javax.swing.JTextField txtIdbelleza;
    public static javax.swing.JTextField txtapellido;
    private javax.swing.JTextField txtnomTratamiento;
    public static javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
