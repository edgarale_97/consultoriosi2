-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-07-2018 a las 15:59:51
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultorio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `belleza`
--

CREATE TABLE `belleza` (
  `idbelleza` int(11) NOT NULL,
  `idpaciente` int(11) NOT NULL,
  `tratamiento_nombre` varchar(50) NOT NULL,
  `aplica` varchar(2500) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `cremas` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `belleza`
--

INSERT INTO `belleza` (`idbelleza`, `idpaciente`, `tratamiento_nombre`, `aplica`, `fecha`, `cremas`) VALUES
(2, 1, 'Inyeccion de plasma', 'Se extrae sangre del paciente, la \nsangre se centrifuga para transformarla\nen plasma y se inyecta en la zona \nafectada', '21/07/2018', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia_paciente`
--

CREATE TABLE `historia_paciente` (
  `idhistoria_paciente` int(11) NOT NULL,
  `idpacientes` int(11) NOT NULL,
  `motivo_consulta` varchar(1000) NOT NULL,
  `enfermedad_reciente` varchar(1000) NOT NULL,
  `antecedentes_personales` varchar(2500) NOT NULL,
  `antecedentes_familiares` varchar(2500) NOT NULL,
  `examen_fisico` varchar(800) NOT NULL,
  `manejo` varchar(800) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `historia_paciente`
--

INSERT INTO `historia_paciente` (`idhistoria_paciente`, `idpacientes`, `motivo_consulta`, `enfermedad_reciente`, `antecedentes_personales`, `antecedentes_familiares`, `examen_fisico`, `manejo`, `estado`) VALUES
(2, 1, 'El paciente presenta un cuadro viral que le dificulta la respiracion, presenta irritacion en la garganta, y dolor en el torax, posible caso de bronquitis', 'Fiebre alta a causa de una infeccion', 'terapia intensiva por un mes a causa de un edema cerebral por el virus Herliquiosi\n', 'Padre impertensivo', 'reflejos excelentes', 'se le coloco 7 dias de antibiotico y 2 semanas de reposo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutricion`
--

CREATE TABLE `nutricion` (
  `idnutricion` int(11) NOT NULL,
  `idpaciente` int(11) NOT NULL,
  `desayuno` varchar(5000) DEFAULT NULL,
  `merienda` varchar(3000) DEFAULT NULL,
  `almuerzo` varchar(5000) DEFAULT NULL,
  `merienda2` varchar(3000) DEFAULT NULL,
  `cena` varchar(5000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nutricion`
--

INSERT INTO `nutricion` (`idnutricion`, `idpaciente`, `desayuno`, `merienda`, `almuerzo`, `merienda2`, `cena`) VALUES
(3, 1, 'Pan integral Con Queso blanco\nPan integral Con Mermelada\nPan integral Con Huevo hervido\nPan integral Con lechuga', 'fruta', 'Arroz integra con pollo a la plancha', 'fruta', 'pan integral');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `idpacientes` int(11) NOT NULL,
  `nombre` varchar(65) NOT NULL,
  `apellido` varchar(65) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `fecha_de_nacimiento` varchar(90) NOT NULL,
  `edad` varchar(20) NOT NULL,
  `peso` varchar(45) NOT NULL,
  `altura` varchar(45) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`idpacientes`, `nombre`, `apellido`, `cedula`, `fecha_de_nacimiento`, `edad`, `peso`, `altura`, `estado`) VALUES
(1, 'Victor Daniel', 'Valente Gouveia', '20792785', '14/12/1992', '25', '60Kg', '1,70m', 1),
(2, 'luis', 'perez', '23221331', '123123', '12', '21', '213', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe_medico`
--

CREATE TABLE `recipe_medico` (
  `idRecipe_medico` int(11) NOT NULL,
  `idpacientes` int(11) NOT NULL,
  `medicina` varchar(45) NOT NULL,
  `indicacion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `idTelefono` int(11) NOT NULL,
  `id_pacientes` int(11) NOT NULL,
  `numero_local` varchar(45) DEFAULT NULL,
  `numero_celular` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`idTelefono`, `id_pacientes`, `numero_local`, `numero_celular`, `estado`) VALUES
(1, 1, '02418427371', '04125096226', 1),
(2, 2, '21321321', '214213213', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `belleza`
--
ALTER TABLE `belleza`
  ADD PRIMARY KEY (`idbelleza`),
  ADD KEY `Fk_belleza_pacientes_idx` (`idpaciente`);

--
-- Indices de la tabla `historia_paciente`
--
ALTER TABLE `historia_paciente`
  ADD PRIMARY KEY (`idhistoria_paciente`),
  ADD KEY `Fk_historia_paciente_paciente_idx` (`idpacientes`);

--
-- Indices de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD PRIMARY KEY (`idnutricion`),
  ADD KEY `Fk_nutricion_pacientes_idx` (`idpaciente`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`idpacientes`);

--
-- Indices de la tabla `recipe_medico`
--
ALTER TABLE `recipe_medico`
  ADD PRIMARY KEY (`idRecipe_medico`),
  ADD KEY `Fk_Recipe_pacientes_idx` (`idpacientes`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`idTelefono`),
  ADD KEY `fk_Telefono_pacientes_idx` (`id_pacientes`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `belleza`
--
ALTER TABLE `belleza`
  MODIFY `idbelleza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `historia_paciente`
--
ALTER TABLE `historia_paciente`
  MODIFY `idhistoria_paciente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  MODIFY `idnutricion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `idpacientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `recipe_medico`
--
ALTER TABLE `recipe_medico`
  MODIFY `idRecipe_medico` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `idTelefono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `belleza`
--
ALTER TABLE `belleza`
  ADD CONSTRAINT `Fk_belleza_pacientes` FOREIGN KEY (`idpaciente`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historia_paciente`
--
ALTER TABLE `historia_paciente`
  ADD CONSTRAINT `Fk_historia_paciente_paciente` FOREIGN KEY (`idpacientes`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD CONSTRAINT `Fk_nutricion_pacientes` FOREIGN KEY (`idpaciente`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recipe_medico`
--
ALTER TABLE `recipe_medico`
  ADD CONSTRAINT `Fk_Recipe_pacientes` FOREIGN KEY (`idpacientes`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `fk_Telefono_pacientes` FOREIGN KEY (`id_pacientes`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
